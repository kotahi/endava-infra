## Infrastructure for eLife Kotahi stack

### Deploy EC2 instance and setup Kotahi docker stack

Requirements:
```
Ansible 2.10.5
Ubuntu 18.04 (on EC2 instance)
```

Prepare the ORCID credentials by https://gitlab.coko.foundation/kotahi/kotahi/-/blob/main/FAQ.md

Define variables in `ansible\vars\default.yml`

Run AWS resources creation
```
cd ansible

ansible-playbook ec2.yml --extra-vars "ansible_ssh_user=ubuntu" --extra-vars "aws_access_key=<aws_access_key>" --extra-vars "aws_secret_key=<aws_secret_key>"
```

What resources the playbook `ec2.yml` deployed:
- AWS VPC,Subnet,IGW,Route IGW
- AWS Security Group
- AWS SSH Key pair (from your public key)
- AWS Elastic IP
- AWS EC2 Instance
- AWS Load Balancer (Application)

DNS: Issue domain name in AWS Certificate Manager and attach TLS certificate to https listener in Load Balancer

Add public_ip in ansible\hosts (see them in AWS console)

Run provision and install Kotahi stack
```
ansible-playbook provision.yml -i hosts --extra-vars "ansible_ssh_user=ubuntu" --extra-vars "aws_access_key=<aws_access_key>" --extra-vars "aws_secret_key=<aws_secret_key>"
```

What things playbook `provision.yml` do:
- Update Ubuntu system repository
- Install needed packages
- Install Deocker and Docker-compose
- Checkout official Kotahi git repository
- Up Kotahi stack in Docker

