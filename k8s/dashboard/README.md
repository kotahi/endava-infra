1. Create Kubernetes Dashboard
```
kubectl create -f k8s/dashboard/recommended.yaml
```

2. Create an eks-admin service account and cluster role binding
```
kubectl create -f k8s/dashboard/eks-admin-service-account.yaml
```

3. Retrieve an authentication token for the eks-admin service account
```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```

4. Start the kubectl proxy
```
kubectl proxy

Starting to serve on 127.0.0.1:8001
```

5. Open  http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=default   
Select "Token" and insert token from step 3