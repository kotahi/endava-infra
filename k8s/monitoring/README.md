Install Helm
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
helm repo add stable https://charts.helm.sh/stable
```

Update k8s cluster context
```
aws --region=us-east-1 eks update-kubeconfig --name eks-<instance_name>-kotahi
```

### Prometheus

Install Prometheus storageclass
```
cd k8s/monitoring/
kubectl create -f prometheus-storageclass.yaml
```

The cluster-admin role is created by default in a Kubernetes cluster, so you don't have to define it explicitly
```
kubectl create -f rbac-config.yaml
```

Install Prometheus
```
helm repo update
kubectl create namespace prometheus
helm install prometheus -f prometheus-values.yaml stable/prometheus --namespace prometheus
```

Bind Prometheus via localhost
```
export POD_NAME=$(kubectl get pods --namespace prometheus -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace prometheus port-forward $POD_NAME 9090
```

Open url http://127.0.0.1:9090/targets


### Grafana

Install Grafana storageclass
```
cd k8s/monitoring/
kubectl create -f grafana-storageclass.yaml
```

Define ingress hosts, secretName and adminPassword in grafana-values-<instance_name>.yaml
```
ingress:
  enabled: true
  annotations: 
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  labels: {}
  path: /
  hosts:
    - <instance_name>-grafana.kotahi.cloud
  extraPaths: []
  tls: 
    - secretName: <instance_name>-grafana-tls
      hosts:
        - <instance_name>-grafana.kotahi.cloud

adminUser: admin
adminPassword: sTrOng&_pa$$wOrD56
```

Install nginx-ingress and Grafana
```
kubectl create namespace grafana
helm install nginx-ingress stable/nginx-ingress --namespace grafana
helm install grafana -f grafana-values-<instance_name>.yaml stable/grafana --namespace grafana
```

Get Grafana Load Balancer domain name
```
kubectl get svc -n grafana | grep nginx-ingress-controller
nginx-ingress-controller        LoadBalancer   172.20.22.216    ac3451f1****************-1006393234.us-east-1.elb.amazonaws.com   80:30366/TCP,443:31873/TCP 
  4h29m
```

Add in dnsimple web ui a record
```
<instance_name>-grafana.kotahi.cloud cname ac3451f1****************-1006393234.us-east-1.elb.amazonaws.com
```

Open https://<instance_name>-grafana.kotahi.cloud/login
- Login: admin
- Password: sTrOng&_pa$$wOrD56

Check ingress and certificate status
```
kubectl get ingress -n grafana
NAME      HOSTS                             ADDRESS                                                                  PORTS     AGE
grafana   <instance_name>-grafana.kotahi.cloud   ac3451f1****************-1006393234.us-east-1.elb.amazonaws.com          80, 443   78m

kubectl get certificate -n grafana
NAME                READY   SECRET              AGE
<instance_name>-grafana-tls   True    <instance>-grafana-tls   78m55s
```

Check unused Grafana http Load Balancer and delete it on AWS console
```
kubectl get svc -n grafana | grep grafana
```

Install grafana-piechart-panel plugin (for 12611 dashboard)
```
kubectl get pods -n grafana
NAME                                             READY   STATUS    RESTARTS   AGE
grafana-7b5ff54785-g7jbz                         1/1     Running   0          8m5s

kubectl exec -ti grafana-7b5ff54785-g7jbz bash -n grafana
grafana-cli plugins install grafana-piechart-panel
exit

kubectl rollout restart deployment grafana -n grafana
```

### Loki & Promtail

Install Loki
```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install loki grafana/loki-stack
```

Check Loki service binding
```
kubectl get services | grep loki
loki                                 ClusterIP      172.20.245.110    3100/TCP
```

Add Loki datasource with url: http://172.20.245.110:3100 in Grafana


### Dashboards

- To monitor k8s cluster import dashboard "Kubernetes Cluster (Prometheus)" https://grafana.com/grafana/dashboards/10670 (Prometheus as datasource)
- To monitor docker containers/services logs import "Loki Dashboard quick search" https://grafana.com/grafana/dashboards/12019 (Loki as datasource)
- To monitor Loki metrics and logs import "Logging Dashboard via Loki" https://grafana.com/grafana/dashboards/12611 (Loki as datasource)

### Kotahi's app logs in realtime
Open needed instance Grafana url:
- https://elife-grafana.kotahi.cloud/explore
- https://ncrc-grafana.kotahi.cloud/explore
- https://colab-grafana.kotahi.cloud/explore

Select "Loki" datasource

Add `{namespace="default", container="server"}` in query field and click button "Live" in top right corner

### Integration Loki logging in Grafana Cloud (https://grafana.com)

- register new account
- add Loki stack (generate Your Grafana.com API Key)
- for adding your private k8s cluster Loki datasource execute command like this:
```
curl -fsS https://raw.githubusercontent.com/grafana/loki/master/tools/promtail.sh | sh -s 45472 <Your Grafana.com API Key> logs-prod-us-central1.grafana.net default | kubectl apply --namespace=default -f  -
```
- open https://<your_account>.grafana.net
- import 12611 dasboard with Loki datasource
- install plugin https://grafana.com/grafana/plugins/grafana-piechart-panel/?tab=installation

Delete Grafana, Loki and Prometheus
```
helm del prometheus -n prometheus
helm del grafana -n grafana
helm del loki
```