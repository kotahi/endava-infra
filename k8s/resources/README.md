### Prerequisites
Installed k8s cluster and added/updated LB cname record in domain settings


Define variables in .env
```
CLIENT_PORT=4000
SERVER_PORT=3000
CLIENT_HOST="0.0.0.0"
PUBSWEET_SECRET=dev_secret
CLIENT_PROTOCOL=http
USE_SANDBOXED_ORCID=false

PUBLIC_CLIENT_PORT=443
PUBLIC_CLIENT_HOST=k8s.kotahi.cloud
PUBLIC_CLIENT_PROTOCOL=https

INSTANCE_NAME=elife
```

Create Helm template
```
kompose convert -c -o elife-kotahi
```

Check files
```
elife-kotahi/ 
├── Chart.yaml
├── README.md
└── templates
    ├── env-configmap.yaml
    ├── job-xsweet-deployment.yaml
    ├── server-deployment.yaml
    └── server-service.yaml
```

Encode ORCID and PUBSWEET credentials
```
echo -n 'orcid_client_id_value' | base64
echo -n 'orcid_client_secret_value' | base64
echo -n 'pubsweet_secret_value' | base64
```

Define in orcid-secret.yaml
```
  ORCID_CLIENT_ID: encrypted_orcid_client_id_value
  ORCID_CLIENT_SECRET: encrypted_orcid_client_secret_value
  PUBSWEET_SECRET: encrypted_pubsweet_secret_value
```

Add cert-manager and create tls-issuer
```
wget https://github.com/jetstack/cert-manager/releases/download/v0.12.0/cert-manager.yaml

vim k8s/helm/elife-kotahi/templates/elife-kotahi-tls-issuer.yaml
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
 name: elife-kotahi-tls-issuer
 namespace: cert-manager
spec:
 acme:
   # The ACME server URL
   server: https://acme-v02.api.letsencrypt.org/directory
   # Email address used for ACME registration
   email: ilia.eriomenco@endava.com
   # Name of a secret used to store the ACME account private key
   privateKeySecretRef:
     name: elife-kotahi-tls-secret
   # Enable the HTTP-01 challenge provider
   solvers:
   - http01:
       ingress:
         class:  nginx
```

Create ingress rules
```
vim k8s/helm/elife-kotahi/templates/server-ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: server-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "elife-kotahi-tls-issuer"
spec:
  tls:
  - hosts:
    - k8s.kotahi.cloud
    secretName: elife-kotahi-tls-secret
  rules:
  - host: k8s.kotahi.cloud
    http:
      paths:
      - backend:
          serviceName: server
          servicePort: 80
```

Create resources of cert-manager
```
kubectl create -f k8s/helm/elife-kotahi/templates/cert-manager.yaml
```

Create Kotahi resources with TLS (https)
```
kubectl create -f k8s/helm/elife-kotahi/templates
```

Check ingress changes
```
kubectl describe ingress
...
Events:
  Type    Reason             Age   From                      Message
  ----    ------             ----  ----                      -------
  Normal  CREATE             38s   nginx-ingress-controller  Ingress default/server-ingress
  Normal  CreateCertificate  38s   cert-manager              Successfully created Certificate "elife-kotahi-tls"
  Normal  UPDATE             2s    nginx-ingress-controller  Ingress default/server-ingress
```

Check generated TLS certificate
```
kubectl get certificates
NAME               READY   SECRET             AGE
elife-kotahi-tls   True    elife-kotahi-tls   2m15s

kubectl describe certificate elife-kotahi-tls
...
Events:
  Type    Reason        Age    From          Message
  ----    ------        ----   ----          -------
  Normal  GeneratedKey  2m44s  cert-manager  Generated a new private key
  Normal  Requested     2m44s  cert-manager  Created new CertificateRequest resource "elife-kotahi-tls-3177350416"
  Normal  Issued        2m3s   cert-manager  Certificate issued successfully
```


Check SSL certificate in https://www.digicert.com/help/
```
TLS Certificate
Common Name = k8s.kotahi.cloud
Subject Alternative Names = k8s.kotahi.cloud
Issuer = R3
OCSP Origin:	Good
TLS Certificate expiration
The certificate expires July 12, 2021 (90 days from today)
TLS Certificate is correctly installed
```

Enable Helm in cluster
```
kubectl create serviceaccount tiller --namespace kube-system
kubectl create -f k8s/helm/elife-kotahi/templates/tiller-clusterrolebinding.yaml
helm init --service-account tiller --upgrade
```

Install Kotahi relaese via HELM (in Jenkins or Gitlab-CI pipeline)
```
helm install ${instance_name}-kotahi-prod ./helm/kotahi \
                    --set images.backend.repository=${env.IMAGE_ORG}/${env.IMAGE_NAME} \
                    --set images.backend.tag=latest \
                    --set images.jobxsweet.repository=pubsweet/job-xsweet \
                    --set images.jobxsweet.tag=1.5.1 \
                    --set backend.deployment.containerPort=3000 \
                    --set backend.ingress.port=80 \
                    --set backend.ingress.targetPort=3000 \
                    --set backend.service.port=80 \
                    --set backend.service.targetPort=3000 \
                    --set rdssecret=elife-kotahi-prod-rds-postgres
```

Deploy/Check/Delete new releases
```
helm upgrade ${instance_name}-kotahi-prod ./helm/kotahi \
                    --set images.backend.repository=${env.IMAGE_ORG}/${env.IMAGE_NAME} \
                    --set images.backend.tag=latest \
                    --set images.jobxsweet.repository=pubsweet/job-xsweet \
                    --set images.jobxsweet.tag=1.5.1 \
                    --set backend.deployment.containerPort=3000 \
                    --set backend.ingress.port=80 \
                    --set backend.ingress.targetPort=3000 \
                    --set backend.service.port=80 \
                    --set backend.service.targetPort=3000 \
                    --set rdssecret=elife-kotahi-prod-rds-postgres                   

helm ls 
NAME                    REVISION        UPDATED                         STATUS                                                                         CHART                    APP VERSION     NAMESPACE
elife-kotahi-prod       1               Fri Apr 23 14:16:53 2021        DEPLOYED                                                                       elife-kotahi-0.1.0       latest          default


helm del elife-kotahi-prod
```



