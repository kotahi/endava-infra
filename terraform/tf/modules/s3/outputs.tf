output "key" {
  value     = aws_iam_access_key.attachment_storage.id
}
output "secret" {
  value     = aws_iam_access_key.attachment_storage.secret
}

output "url" {
  value    =  aws_s3_bucket.attachment_storage.bucket_domain_name
}