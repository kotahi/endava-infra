resource "aws_s3_bucket" "attachment_storage" {
  bucket = var.bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
}

resource "aws_iam_user" "attachment_storage" {
  name = "${var.bucket_name}-access-user"
  path = "/system/"
}

resource "aws_iam_access_key" "attachment_storage" {
  user = aws_iam_user.attachment_storage.name
}

resource "aws_iam_user_policy" "attachment_storage_rw" {
  name = "${var.bucket_name}-storage-rw"
  user = aws_iam_user.attachment_storage.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:ListBucket",
        "s3:GetObject",
		    "s3:PutObject",
		    "s3:GetObjectVersion"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.attachment_storage.arn}/*",
        "${aws_s3_bucket.attachment_storage.arn}"
	  ]
    }
  ]
}
EOF
}