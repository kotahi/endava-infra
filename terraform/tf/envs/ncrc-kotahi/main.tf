provider "aws" {
  region = var.region
}

# Needed for the terraform-aws-eks module to create the aws-auth ConfigMap
provider "kubernetes" {
  host = module.eks.kubernetes_config.host
  cluster_ca_certificate = module.eks.kubernetes_config.cluster_ca_certificate
  token =  module.eks.kubernetes_config.token
  load_config_file       = false
  version                = "~> 1.10"
}

provider "helm" {
  kubernetes {
    host = module.eks.kubernetes_config.host
    cluster_ca_certificate = module.eks.kubernetes_config.cluster_ca_certificate
    token =  module.eks.kubernetes_config.token
    load_config_file = false
  }
  version = "~> 1.0"
}

provider "kubernetes-alpha" {
  host = module.eks.kubernetes_config.host
  cluster_ca_certificate = module.eks.kubernetes_config.cluster_ca_certificate
  token =  module.eks.kubernetes_config.token
  version = "~> 0.2"
}

provider "google" {
  project = "ncrc-kotahi"
  version = "3.56.0"
}

terraform {
  backend "s3" {
    bucket = "ncrc-kotahi-terraform"
    # specify with terraform init --backend-config="key=<env>/terraform.tfstate" to make it dynamic
    #key    = "unstable/terraform.tfstate"
    region = "us-east-1"
  }
  required_version = ">= 0.12.0"
}

locals {
  cluster_name        = "eks-${var.env}"
  storage_bucket_name = "${var.env}-attachment-storage"
}

module "vpc" {
  source = "../../modules/vpc"
  cluster_name = local.cluster_name
}

module "eks" {
  source = "../../modules/eks"
  cluster_name = local.cluster_name
  env = var.env
  vpc_id = module.vpc.vpc_id
  subnets = module.vpc.subnets
  map_users = var.map_users
}

module "ingress" {
  source = "../../modules/ingress"
}

module "rds" {
  source = "../../modules/rds_db"
  database_id = "${var.env}-prod-postgres"
  accessing_security_group_id = module.eks.worker_security_group_id
  vpc_id = module.vpc.vpc_id
  subnet_ids = module.vpc.subnets
}

resource "kubernetes_secret" "rds_postgres" {
  metadata {
    name = "${var.env}-prod-rds-postgres"
  }
  data = {
    postgresql-database = module.rds.database
    postgresql-username = module.rds.username
    postgresql-password = module.rds.password
    postgresql-host = module.rds.hostname
    postgresql-port = module.rds.port
  }
}

resource "kubernetes_secret" "attachment_storage" {
  metadata {
    name = "${var.env}-prod-s3-attachment"
  }
  data = {
    S3_REGION            = "${var.region}"
    S3_ACCESS_KEY_ID     = module.attachment_storage.key
    S3_ACCESS_KEY_SECRET = module.attachment_storage.secret
    S3_ENDPOINT          = module.attachment_storage.url
  }
}

module "attachment_storage" {
  source        = "../../modules/s3"
  bucket_name   = local.storage_bucket_name
}

resource "null_resource" "db_setup" {

  provisioner "local-exec" {

    command = "psql -h ${module.rds.hostname} -p ${module.rds.port} -U ${module.rds.username} -d ${module.rds.database} -c 'CREATE EXTENSION pgcrypto;'"

    environment = {
      PGPASSWORD = "${module.rds.password}"
    }
  }
  depends_on = [module.rds]
}



