variable "region" {
  default = "us-east-1"
  description = "The AWS region."
}

variable "env" {
  default = "ncrc-kotahi"
  description = "Environment: prod, dev, ..."
}

variable "domain" {
  default = "ncrc.kotahi.cloud"
  description = "The domain."
}

variable "map_users" {
  default = [
    {
      userarn  = "arn:aws:iam::381875417048:user/PaulShannon"
      username = "PaulShannon"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::381875417048:user/IliaEriomenco"
      username = "IliaEriomenco"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::381875417048:user/ElifeAdmin"
      username = "ElifeAdmin"
      groups   = ["system:masters"]
    },
  ]
  description = "IAM users that can access the cluster"
}
