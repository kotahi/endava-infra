# output "vpc_id" {
#   value = module.vpc.vpc_id
#   description = "ID of the generated VPC e.g. vpc-1234567890"
# }

# output "subnets" {
#   value = module.vpc.subnets
#   description = "List of the public subnets created in the VPC"
# }

# output "worker_iam_role_name" {
#   value = module.eks.worker_iam_role_name
#   description = "Name of the role, e.g. Accounting-Role"
# }

# output "worker_security_group_id" {
#   value = module.eks.worker_security_group_id
#   description = "Security group ID attached to the EKS workers, e.g. sg-018caeb91524d23ff"
# }

# output "kubernetes_config" {
#   value = module.eks.kubernetes_config
#   description = "Kubernetes provider configuration"
# }

# output "rds_database" {
#   value = module.rds.database
#   description = "Database name"
# }

# output "rds_hostname" {
#   value = module.rds.hostname
#   description = "Database hostname"
# }

# output "rds_password" {
#   value = module.rds.password
#   description = "Database password"
# }

# output "rds_port" {
#   value = module.rds.port
#   description = "Database port"
# }

# output "rds_username" {
#   value = module.rds.username
#   description = "Database username"
# }

# output "attachment_storage_key" {
#   value = module.attachment_storage.key
#   description = "S3 attachment bucket IAM access key"
# }

# output "attachment_storage_secret" {
#   value = module.attachment_storage.secret
#   description = "S3 attachment bucket IAM access secret"
# }

# output "attachment_storage_url" {
#   value = module.attachment_storage.url
#   description = "S3 attachment bucket domain name"
# }