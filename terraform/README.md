# Infrastructure
Infrastructure as Code for Kotahi

# Requirements
Terraform 0.14.8

## Environments

- `elife-kotahi`: Kubernetes cluster for eLife instance.
- `ncrc-kotahi`: Kubernetes cluster for NCRC instance.
- `colab-kotahi`: Kubernetes cluster for Colab instance.

## Scope

Resources belonging to a elife AWS account are managed here:

### Kubernetes

Kubernetes clusters to deploy vanilla versions of eLife products and modules are housed here.

They run in their own VPCs in `us-east-1`.

### Servers

EC2 instances are used to demonstrate working versions of eLife's projects.

They run inside the default VPC for `us-east-1` and are based on a minimal Ubuntu OS running Docker and `docker-compose`. Software is deployed in the form of container images.


## Setup


### Terraform usage

You will need to set up AWS credentials in your ~/.aws/credentials file:

```
[default]
aws_access_key_id=<access key id>
aws_secret_access_key=<secret access key>
```

### New environment

```
cd tf/envs/
mkdir $ENVIRONMENT_NAME
define env, domain etc in $ENVIRONMENT_NAME/variables.tf
terraform init --backend-config="key=$ENVIRONMENT_NAME/terraform.tfstate"
terraform plan -out=$ENVIRONMENT_NAME.plan
terraform apply $ENVIRONMENT_NAME.plan
```

### Fetch current environment state and get output
```bash
cd terraform/tf/envs/$ENVIRONMENT_NAME
rm -rf .terraform
terraform init --backend-config="key=$ENVIRONMENT_NAME/terraform.tfstate"
terraform output
```

### Kubectl access

Assuming you have AWS credentials setup to access the elife account, the `aws` cli can generate your `kubectl` authentication configuration for the cluster:

```
aws eks update-kubeconfig --name eks-$ENVIRONMENT_NAME --region us-east-1 --alias $ENVIRONMENT_NAME

kubectl get pods
```

### DNS

Add `elife.kotahi.cloud`/`ncrc.kotahi.cloud`/`biophysics-sciencecolab.kotahi.cloud` in DNSimple in kotahi.cloud hosted zone

Link AWS Load Balancer DNS name a CNAME record for every domain


