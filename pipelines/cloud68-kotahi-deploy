pipeline {
    agent {label 'master'}
    options{
        buildDiscarder(logRotator(numToKeepStr: '25'))
    }
      
   parameters{
        choice(
            name: 'instance_name',
                choices:
                    ['elife',
                     'ncrc',
                     'colab',
                    ],
                description: '')  
   }

   environment {
        IMAGE_ORG = "kotahi"
        IMAGE_NAME = "kotahi-${instance_name}"
        GIT_COMMIT_HASH = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
    }

stages {

    stage('Initialize'){
        steps{
            echo "Initialization"
                script {
                    manager.addShortText("${instance_name}", "black", "lightsalmon", "0px", "white")
                }
        }
    }
        
    stage('Checkout'){
        steps {
             cleanWs()
             checkout([$class: 'GitSCM',
                branches: [[name: '*/main' ]],
                extensions: scm.extensions,
                userRemoteConfigs: [[
                    url: 'https://gitlab.coko.foundation/kotahi/kotahi.git'
                ]]
            ])
        }
    }

    stage('Get dev and prod node_modules from S3 and unzip them'){
        steps {
            withAWS(credentials:'ELIFE_AWS_ACCESS') {
            sh """
               apt-get update && sudo apt-get install unzip awscli -y
               aws s3 cp s3://elife-node-modules/node_modules_dev.tar.gz .
            """
            }
           
        }
    }

    stage('Build'){
        steps {
            withCredentials([usernamePassword(credentialsId: 'DOCKERHUB_CREDENTIALS_ELIFE', usernameVariable: 'DOCKERHUB_USER_ELIFE', passwordVariable: 'DOCKERHUB_PASS_ELIFE')]) {
                script {
                        if (env.instance_name == 'colab') {
                            sh """
                                echo ${env.DOCKERHUB_USER_ELIFE}
                                docker login -u ${env.DOCKERHUB_USER_ELIFE} -p ${env.DOCKERHUB_PASS_ELIFE}
                                cp app/brand-instances-configs/${instance_name}.json app/brandConfig.json
                                docker build \
                                --build-arg instance_name=${instance_name} \
                                --build-arg manuscripts_table_columns=meta.title,created,updated,status,submission.labels,author \
                                --build-arg public_client_host=colab.cloud68.co \
                                --build-arg public_client_port=443 \
                                --build-arg public_client_protocol=https \
                                --file ./Dockerfile-production-elife \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH} \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest .
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH}
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest
                                rm -rf node_modules_prod.tar.gz node_modules_dev.tar.gz
                            """
                        } else if (env.instance_name == 'elife') {
                            sh """
                                echo ${env.DOCKERHUB_USER_ELIFE}
                                docker login -u ${env.DOCKERHUB_USER_ELIFE} -p ${env.DOCKERHUB_PASS_ELIFE}
                                cp app/brand-instances-configs/${instance_name}.json app/brandConfig.json
                                docker build \
                                --build-arg instance_name=${instance_name} \
                                --build-arg manuscripts_table_columns=submission.articleId,created,updated,status,author \
                                --build-arg public_client_host=elife.cloud68.co \
                                --build-arg public_client_port=443 \
                                --build-arg public_client_protocol=https \
                                --file ./Dockerfile-production-elife \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH} \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest .
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH}
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest
                                rm -rf node_modules_prod.tar.gz node_modules_dev.tar.gz
                            """
                        } else {
                            sh """
                                echo ${env.DOCKERHUB_USER_ELIFE}
                                docker login -u ${env.DOCKERHUB_USER_ELIFE} -p ${env.DOCKERHUB_PASS_ELIFE}
                                cp app/brand-instances-configs/${instance_name}.json app/brandConfig.json
                                docker build \
                                --build-arg instance_name=${instance_name} \
                                --build-arg manuscripts_table_first_column_width=25% \
                                --build-arg manuscripts_table_columns=submission.articleDescription,submission.journal,created,updated,submission.topics,status,submission.labels,editor \
                                --build-arg public_client_host=ncrc.cloud68.co \
                                --build-arg public_client_port=443 \
                                --build-arg public_client_protocol=https \
                                --file ./Dockerfile-production-elife \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH} \
                                --tag ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest .
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:${GIT_COMMIT_HASH}
                                docker push ${env.IMAGE_ORG}/${env.IMAGE_NAME}:latest
                                rm -rf node_modules_prod.tar.gz node_modules_dev.tar.gz
                            """
                        }
                    }            
                }
            }
    }

    stage('Deploy'){
        steps{ 
            withCredentials([sshUserPrivateKey(credentialsId: "CLOUD68_SSH_CREDENTIALS", keyFileVariable: "CLOUD68_SSH_KEY")]) {
                sh '''
                   ssh -o StrictHostKeyChecking=no ci@${instance_name}.cloud68.co -p 42636 -i $CLOUD68_SSH_KEY "docker pull kotahi/kotahi-${instance_name}:latest && \
                   sudo systemctl restart kotahi.service"
                '''               
            }               
        }
    }  
}
 
    post {
        failure {
            withCredentials([string(credentialsId: 'ELIFE_MATTERMOST_WEBHOOK', variable: 'mattermost_endpoint')]){
                mattermostSend(color: 'good',
                icon: "https://jenkins.io/images/logos/jenkins/jenkins.png",
                message: "*${env.JOB_NAME}* (<${env.BUILD_URL}|#${env.BUILD_NUMBER}>) finished with status ${currentBuild.currentResult}",
                channel: "elife-kotahi-alerts",
                endpoint: "${mattermost_endpoint}") 
            }                        
        }
    }
}

