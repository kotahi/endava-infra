![AWS Resources](img/eLife_Kotahi_AWS_Resources.png)

![Kotahi deploy](img/eLife_Kotahi_deploy.png)

## Initial creation AWS/k8s resources for eLife/NCRC/Colab and launch EKS cluster from scratch

### Prerequisites
Locally installed kubectl (for creating initial k8s resources)

Create k8s manifests in k8s/resources/<instance_name>-kotahi/templates
```
cert-manager.yaml -> ../cert-manager.yaml
└── templates
    ├── env-configmap.yaml
    ├── orcid-secret.yaml
    ├── server-ingress.yaml
    ├── server-service.yaml
    └── tls-issuer.yaml
```

Encode ORCID, PUBSWEET, HYPOTHESIS_API_KEY, GOOGLE_SPREADSHEET, CROSSREF credentials and put them in orcid-secret.yaml
```
echo -n 'orcid_client_id_value' | base64
echo -n 'orcid_client_secret_value' | base64
echo -n 'pubsweet_secret_value' | base64
echo -n 'hypothesis_api_key_value' | base64
echo -n 'google_spreadsheet_id' | base64
echo -n 'google_spreadsheet_client_email_value' | base64
echo -n 'google_spreadsheet_private_key_value' | base64
echo -n 'crossref_login_value' | base64
echo -n 'crossref_password_value' | base64
echo -n 's3_bucket_value' | base64
echo -n 'gmail_notification_email_auth' | base64
echo -n 'gmail_notification_email_sender' | base64
echo -n 'gmail_notification_password' | base64
```

orcid-secret.yaml variables template
```
apiVersion: v1
kind: Secret
metadata:
  name: orcid-secret
data:
  ORCID_CLIENT_ID: 
  ORCID_CLIENT_SECRET: 
  PUBSWEET_SECRET: 
  HYPOTHESIS_API_KEY:
  GOOGLE_SPREADSHEET_ID: (this variable use on NCRC only, it's fake for eLife/Colab)
  GOOGLE_SPREADSHEET_CLIENT_EMAIL: 
  GOOGLE_SPREADSHEET_PRIVATE_KEY:
  CROSSREF_LOGIN:
  CROSSREF_PASSWORD: 
  S3_BUCKET: 
  GMAIL_NOTIFICATION_EMAIL_AUTH:
  GMAIL_NOTIFICATION_EMAIL_SENDER:
  GMAIL_NOTIFICATION_PASSWORD:
```

Add <instance_name> in parameters.choice.instance_name in pipelines/aws-kotahi-setup

In AWS console create S3 bucket for Terraform state storage (<instance_name>-kotahi-terraform)

### Auto deploy AWS resources with Jenkins
 
1. Create Jenkins pipeline (pipeline location pipelines/aws-kotahi-setup)
2. Select needed instance_name from dropdown (elife,ncrc,colab)
3. Click build with parameters

### Auto deploy AWS resources with Gitalab-CI
 
1. Define TF_ENV variable in .gitlab-ci.yml
2. Uncomment 23-26 lines (terraform init/plan/apply/state) in .gitlab-ci.yml
3. Push your changes in "endava-infra" Gitlab repository
4. See pipeline output in https://gitlab.coko.foundation/kotahi/endava-infra/-/pipelines

What resources will deployed after pipeline execution:
- AWS VPC, Subnet, IGW, Route IGW, Security Group
- AWS EKS cluster (3 EC2 t2.small instances, Autoscaling group)
- AWS Load Balancer (classic) for EKS cluster
- AWS RDS Postgresql instance
- AWS S3 bucket for Kotahi attachments
- AWS IAM user/credentials for S3 bucket

## IMPORTANT!

Prerequisites before deploy initial k8s resources

See EKS Load Balancer dns name in https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LoadBalancers:sort=loadBalancerName

Add in DNSsimple web ui the record like this
```
<instance_name>.kotahi.cloud cname xxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxxxxxx.us-east-1.elb.amazonaws.com
```

### Deploy initial k8s resources

update kubeconfig
```
aws --region=us-east-1 eks update-kubeconfig --name eks-<instance_name>-kotahi
```

create initial k8s resources
```
kubectl create -f k8s/resources/<instance_name>-kotahi/cert-manager.yaml 
kubectl create -f k8s/resources/<instance_name>-kotahi/templates
```

check initial resources
```
kubectl get configmaps
NAME                              DATA   AGE
env                               6      7m56s

kubectl get secrets | grep <instance_name>
<instance_name>-kotahi-prod-rds-postgres         Opaque                                5      65m
<instance_name>-kotahi-prod-s3-attachment        Opaque                                4      74m
<instance_name>-kotahi-tls                       kubernetes.io/tls                     3      5m23s

kubectl get certificate
NAME                         READY              SECRET            AGE
<instance_name>-kotahi-tls   True    <instance_name>-kotahi-tls   4m24s

kubectl describe ingress
...
TLS:
  <instance_name>-kotahi-tls terminates <instance_name>.kotahi.cloud
Rules:
  Host                   Path  Backends
  ----                   ----  --------
  <instance_name>.kotahi.cloud
                            server:80 (<none>)
Annotations:             cert-manager.io/cluster-issuer: letsencrypt-prod
                         kubernetes.io/ingress.class: nginx
Events:
  Type    Reason             Age    From                      Message
  ----    ------             ----   ----                      -------
  Normal  CREATE             8m37s  nginx-ingress-controller  Ingress default/server-ingress
  Normal  CreateCertificate  8m37s  cert-manager              Successfully created Certificate "<instance_name>-kotahi-tls"
  Normal  UPDATE             7m55s  nginx-ingress-controller  Ingress default/server-ingress
```

### Initial auto deploy Kotahi stack (helm install) with Jenkins

1. Create Jenkins pipeline (pipeline location pipelines/k8s-kotahi-setup)
2. Select needed instance_name from dropdown (elife,ncrc,colab)
3. Click build with parameters

Check pods, deployments status and revision number
```
kubectl get pods
NAME                                        READY   STATUS                  RESTARTS   AGE
job-xsweet-7585745445-thfww                 1/1     Running                 0          8m
server-5fc7bdb9f9-fzrvg                     1/1     Running                 0          8m

kubectl get deployment
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
job-xsweet                 1/1     1            1           8m
server                     1/1     1            1           8m
```

### Regular auto deploy Kotahi stack (helm upgrade) with Jenkins

1. Create Jenkins pipeline (pipeline location pipelines/k8s-kotahi-deploy)
2. Select needed instance_name from dropdown (elife,ncrc,colab)
3. Click build with parameters

Check Kotahi app by url https://<instance_name>.kotahi.cloud

### Troubleshooting
Error:
```
{"graphQLErrors":[{"name":"Server Error","message":"Something went wrong! Please contact your administrator"}],"networkError":null,"message":"Something went wrong! Please contact your administrator"}
```

Reason: isn't installed pgcrypto extention in database

How to solve this manually:
- AWS console > RDS > Databases > <instance_name>-kotahi-prod-postgres > Modify > Connectivity > Additional configuration > Publicly accessible > Continue > Modify DB instance
- AWS console > RDS > Databases > <instance_name>-kotahi-prod-postgres > Security group rules > EC2 Security Group - Inbound > Edit > Add rule > port: 5432, source: your ip
- Connect to database with SQL client and execute query `CREATE EXTENSION pgcrypto;`
- AWS console > RDS > Databases > <instance_name>-kotahi-prod-postgres > Modify > Connectivity > Additional configuration > Not publicly accessible > Continue > Modify DB instance

For solving this problem automaticaly created the following resource in Terraform https://gitlab.coko.foundation/kotahi/endava-infra/-/blob/master/terraform/tf/envs/ncrc-kotahi/main.tf#L107-118

### kubectl/helm cheet sheet

get clusters names (contexts)
```
kubectl config get-contexts

CLUSTER
arn:aws:eks:us-east-1:xxxxxxxxxxxxxxx:cluster/eks-colab-kotahi
arn:aws:eks:us-east-1:xxxxxxxxxxxxxxx:cluster/eks-ncrc-kotahi
arn:aws:eks:us-east-1:xxxxxxxxxxxxxxx:cluster/eks-elife-kotahi
```

switch to needed context
```
kubectl config use-context arn:aws:eks:us-east-1:xxxxxxxxxxxxxxx:cluster/eks-colab-kotahi
```

get deployments list
```
helm ls
```

get deployment/release hystory
```
helm history <instance_name>-kotahi-prod
```

get deployment/release values
```
helm get values <instance_name>-kotahi-prod
```


### Kubernetes Dashboard and Monitoring
- [Dashboard](./k8s/dashboard/README.md)
- [Monitoring](./k8s/monitoring/README.md)

### Manual AWS resources deleting (AWS console)

Order:
- RDS instance
- EKS cluster
- EC2 > Load Balancers > EKS Load Balancer
- EC2 > Auto Scaling groups > eks-<instance_name>-kotahi-eks-<instance_name>-kotahi-small
- IAM > Users > <instance_name>-kotahi-attachment-storage-access-user
- S3 > <instance_name>-kotahi-attachment-storage
- S3 > <instance_name>-kotahi-terraform

